package com.lab.testsystem.services;

import com.lab.testsystem.dao.TestResultDao;
import com.lab.testsystem.model.TestResult;
import lombok.NonNull;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.SQLException;
import java.sql.SQLInvalidAuthorizationSpecException;
import java.sql.SQLSyntaxErrorException;
import java.sql.SQLWarning;
import java.util.List;
import java.util.Objects;

@Log4j
@Component
public class TestResultServiceImplementation implements TestResultService {

    private TestResultDao testResultDaoService;

    @Autowired
    public TestResultServiceImplementation(TestResultDao daoService) {
        testResultDaoService = daoService;
    }

    @Override
    public TestResult save(TestResult testResult) {
        if (Objects.isNull(testResult)) throw new IllegalArgumentException();
        try {
            return testResultDaoService.create(testResult);
        } catch (SQLException e) {
            log.error("Something went wrong with " + testResult.toString());
            throw sqlExceptionCatchHelper(e);
        } catch (IllegalArgumentException e) {
            log.error("test result is not valid, testResult won't be saved " + testResult.toString());
            throw e;
        }
    }

    @Override
    public TestResult get(long id) {
        if (Long.compare(id, 0L) == 0) throw new IllegalArgumentException();
        try {
            return testResultDaoService.get(id);
        } catch (SQLException e) {
            log.error("Something went wrong with this Test Result (id) = " + id);
            throw sqlExceptionCatchHelper(e);
        }
    }

    @Override
    public List<TestResult> get() {
        try {
            return testResultDaoService.get();
        } catch (SQLException e) {
            log.error("query is not valid");
            throw sqlExceptionCatchHelper(e);
        }
    }

    @Override
    public TestResult delete(TestResult testResult) {
        if (Objects.isNull(testResult)) throw new IllegalArgumentException();
        try {
            return testResultDaoService.delete(testResult);
        } catch (SQLException e) {
            log.error("Something went wrong with " + testResult.toString());
            throw sqlExceptionCatchHelper(e);
        } catch (IllegalArgumentException e) {
            log.error("test result is not valid, testResult won't be deleted " + testResult.toString());
            throw e;
        }
    }

    /**
     * all exceptions are logging and wrapping into runtime exception
     * @param e an initial checked SQLException, came from database exception (cannot be null)
     * @return new custom runtime unchecked exception with cause = initial exception (e)
     */
    private DatabaseAccessException sqlExceptionCatchHelper(@NonNull SQLException e) {
        if (e.getClass() == SQLWarning.class) {
            log.warn("There is warning in the query line " + e);
        } else if (e.getClass() == SQLSyntaxErrorException.class) {
            log.error("SQL syntax error in the query " + e);
        } else if (e.getClass() == SQLInvalidAuthorizationSpecException.class) {
            log.error("invalid authorization (maybe illegal credentials) " + e);
        } else {
            log.error("Exception " + e.getClass() + " with this error code "  + e.getErrorCode());
        }
        throw new DatabaseAccessException(e);
    }
}
