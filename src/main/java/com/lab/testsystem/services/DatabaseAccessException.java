package com.lab.testsystem.services;

import java.sql.SQLException;

public class DatabaseAccessException extends RuntimeException {

    public DatabaseAccessException(SQLException cause) {}
}
