package com.lab.testsystem.model;

public enum Role {
    STUDENT,
    TUTOR,
    UNKNOWN
}
