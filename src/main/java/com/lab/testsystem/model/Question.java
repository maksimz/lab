package com.lab.testsystem.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(exclude = {"questionText", "answers"})
public class Question {
    private long id;
    private String questionText;
    private List<Answer> answers;

}