package com.lab.testsystem.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(exclude = {"name", "surname", "login", "password", "role"})
public class User {
    private long id;
    private String name;
    private String surname;
    private String login;
    private String password;
    private Role role;
}