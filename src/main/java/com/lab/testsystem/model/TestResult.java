package com.lab.testsystem.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Map;

@Data
@EqualsAndHashCode(exclude = {"test", "results", "user"})
public class TestResult {
    private long testResultId;
    private Test test;
    private Map<Question, Boolean> results;
    private User user;
}