package com.lab.testsystem.dao;

import com.lab.testsystem.model.TestResult;
import org.springframework.stereotype.Component;

import java.sql.SQLException;
import java.util.List;

@Component
public class TestResultDaoImplementation implements TestResultDao {
    @Override
    public TestResult create(TestResult testResult) throws SQLException {
        return null;
    }

    @Override
    public TestResult get(long id) throws SQLException {
        return null;
    }

    @Override
    public TestResult delete(TestResult testResult) throws SQLException {
        return null;
    }

    @Override
    public List<TestResult> get() throws SQLException {
        return null;
    }
}
