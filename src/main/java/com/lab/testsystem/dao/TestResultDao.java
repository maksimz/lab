package com.lab.testsystem.dao;

import com.lab.testsystem.model.TestResult;

import java.sql.SQLException;
import java.util.List;

public interface TestResultDao {
    TestResult create(TestResult t) throws SQLException;

    TestResult delete(TestResult t) throws SQLException;

    List<TestResult> get() throws SQLException;

    TestResult get(long id) throws SQLException;
}
