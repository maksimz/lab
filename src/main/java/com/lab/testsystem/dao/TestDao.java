package com.lab.testsystem.dao;

import com.lab.testsystem.model.Test;

import java.sql.SQLException;
import java.util.List;

public interface TestDao {

    Test create(Test t) throws SQLException;

    Test delete(Test t) throws SQLException;

    List<Test> get() throws SQLException;

    Test get(long id) throws SQLException;
}
