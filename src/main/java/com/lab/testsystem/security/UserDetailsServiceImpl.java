package com.lab.testsystem.security;

import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.Collections;

public class UserDetailsServiceImpl implements UserDetailsService {

    /**
     * Loads user from a database
     * @throws UsernameNotFoundException if user not found
     */
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        // If database contains username

        // Test data
        if (username.equals("afanasii")) {
            return new User(
                    username,
                    "12345",
                    Collections.singleton(new SimpleGrantedAuthority("ROLE_TUTOR")));
        }

        if (username.equals("przemyslaw")) {
            return new User(
                    username,
                    "12345",
                    Collections.singleton(new SimpleGrantedAuthority("ROLE_STUDENT")));
        }

        throw new UsernameNotFoundException(username);
    }
}
