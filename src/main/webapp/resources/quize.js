
var saveQuestions = function () {
    $.ajax({
        type:"POST",
        url:"/saveTest",
        data: JSON.stringify(assembleQuize()),
        contentType:"application/json",

        success: function (url) {
            window.location.href = url;
        },
        error: function () {
            console.log("Save questions is failed");
        }
    })
}



var assembleQuize = function () {
    var quize = {title : 'default title', questions : []};
    quize.title = document.getElementById('test-title').innerText
    quize.questions = assembleQuestions();
    return quize;
}
var assembleQuestions = function () {
    var questions = [];
    var i = 1;
    while (true) {
        var questionId = 'question-' + i;
        var questionElement = document.getElementById(questionId);

        if (questionElement == undefined) break;

        var question = {questionText : 'default', answers : []};

        question.questionText = questionElement.value;
        question.answers = assembleAnswers(questionId);
        questions.push(question);
        i++;
    }
    return questions;
}

var assembleAnswers = function (questionId) {
    var answers = [];
    var i = 1;
    while (true) {
        var isAnswerCorrectElement = document.getElementById('is-answer-for-' + questionId + '-correct-' + i);
        var answerElement = document.getElementById('answer-for-' + questionId + '-' + i);

        if (isAnswerCorrectElement == undefined && answerElement == undefined) break;
        answers.push({textAnswer: answerElement.value, isCorrect: isAnswerCorrectElement.checked})
        i++;
    }
    return answers;
}