<%@ page contentType="text/html;charset=UTF-8" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title><spring:message code="create_test_title"/></title>

    <link rel='stylesheet prefetch'
          href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha.6/css/bootstrap.min.css'>
    <link rel='stylesheet prefetch'
          href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css'>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="resources/css/createTest.css">

</head>
<body>
<span style="float: right; size: 12px; color:white">
                    <a href="?lang=en" style="size: 12px; color: white;">en</a>
                            |
                    <a href="?lang=ru" style="size: 12px; color: white;">ru</a>
                </span>
<div class="container">
    <div class="row main">

        <div class="main-login main-center">
            <form class="form-horizontal" method="POST" action="">

                <div class="test-title">
                    <p><spring:message code="create_test_title"/></p>
                </div>

                <div class="form-group">
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-book"
                                                               aria-hidden="true"></i></span>
                            <input type="text" class="form-control" name="Title" id="title"
                                   placeholder="<spring:message code="create_test_title_input"/>"
                                   size="200"/>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-pencil"
                                                               aria-hidden="true"></i></span>
                            <input type="number" class="form-control" name="numberOfQuestions"
                                   id="numberOfQuestions"
                                   placeholder="<spring:message code="create_test_title_number_of_questions"/>"/>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-pencil"
                                                               aria-hidden="true"></i></span>
                            <input type="number" class="form-control" name="numberOfAnswers"
                                   id="numberOfAnswers" placeholder="<spring:message
                                   code="create_test_title_number_of_variants"/>"/>
                        </div>
                    </div>
                </div>

                <div class="button">
                </div>
                <button class="btn btn-primary btn-md btn-block
                submit-button"><spring:message code="create_test_title_button"/></button>
            </form>
        </div>
    </div>
</div>
</body>
</html>