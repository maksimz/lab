<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<%--
  Created by IntelliJ IDEA.
  User: raya
  Date: 13/07/2018
  Time: 15:06
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>Add Question</title>
    <link rel='stylesheet prefetch'
          href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha.6/css/bootstrap.min.css'>
    <link rel='stylesheet prefetch'
          href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css'>
    <link rel="stylesheet" type="text/css" href="resources/css/addQuestion.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="/resources/quize.js"></script>

</head>
<body>
<span style="float: right; size: 12px; color:white">
                    <a href="?lang=en" style="size: 12px; color: white;">en</a>
                            |
                    <a href="?lang=ru" style="size: 12px; color: white;">ru</a>
                </span>
<div class="container">
    <div class="row main">

        <div class="main-login main-center">
            <form class="form-horizontal" method="POST" action="/saveTest"
                  enctype="application/json">

                <div class="test-title">
                    <p id="test-title">Test title: '${testTitle}'</p>
                </div>
                <div class="test-title">
                    <p><spring:message code="create_test_title_number_of_questions"/></p>
                </div>
                <c:forEach var="i" begin="1" end="${questionCount}" step="1">
                    <div class="form-group">
                        <label for="question-${i}" class="cols-sm-2 control-label">Question
                            "${i}"</label>
                        <div class="cols-sm-10">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-pencil"
                                                                   aria-hidden="true"></i></span>
                                <input type="text" class="form-control" name="question"
                                       id="question-${i}" placeholder="Enter your Question"/>
                            </div>
                        </div>
                    </div>

                    <table>
                        <c:forEach var="b" begin="1" end="${answerCount}" step="1">
                            <tr height="50px">
                                <td><input id="is-answer-for-question-${i}-correct-${b}"
                                           type="checkbox"
                                           value=""></td>
                                <td><label for="is-answer-for-question-${i}-correct-${b}"><textarea
                                        class="variant"
                                        id="answer-for-question-${i}-${b}"></textarea></label>
                                </td>
                            </tr>
                        </c:forEach>
                    </table>
                </c:forEach>
                <div class="button">
                    <button type="button" onclick="saveQuestions()"
                            class="btn btn-sm btn-outline-success">Add
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
</body>
</html>
