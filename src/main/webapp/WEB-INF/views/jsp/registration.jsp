<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%@ page contentType="text/html;charset=UTF-8" %>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title><spring:message code="registration_page_title"/></title>

    <link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha.6/css/bootstrap.min.css'>
    <link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css'>
    <link rel="stylesheet" type="text/css" href="resources/css/login.css">

</head>

<body>
<div class="container">
    <span style="float: right; size: 12px; color:white">
                    <a href="?lang=en" style="size: 12px; color: white;">en</a>
                            |
                    <a href="?lang=ru" style="size: 12px; color: white;">ru</a>
                </span>
    <div class="row main">

        <div class="main-login main-center">
            <form class="form-horizontal" method="post" action="#">
                <div class="form-group">
                    <label for="name" class="cols-sm-2 control-label"><spring:message code="registration_page_first_name"/></label>
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                            <input type="text" class="form-control" name="name" id="name" placeholder="<spring:message code='registration_page_first_name_input_title'/>" size="40"/>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="surname" class="cols-sm-2 control-label"><spring:message code="registration_page_second_name"/></label>
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                            <input type="text" class="form-control" name="surname" id="surname" placeholder="<spring:message code='registration_page_second_name_input_title'/>" />
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="email" class="cols-sm-2 control-label"><spring:message code="registration_page_email"/></label>
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
                            <input type="text" class="form-control" name="email" id="email" placeholder="<spring:message code='registration_page_email_input_title'/>" />
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="username" class="cols-sm-2 control-label"><spring:message code="registration_page_username"/></label>
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-users fa" aria-hidden="true"></i></span>
                            <input type="text" class="form-control" name="username" id="username" placeholder="<spring:message code='registration_page_username_input_title'/>" />
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="password" class="cols-sm-2 control-label"><spring:message code="registration_page_password"/></label>
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
                            <input type="password" class="form-control" name="password" id="password" placeholder="<spring:message code='registration_page_password_input_title'/>" />
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="confirm" class="cols-sm-2 control-label"><spring:message code="registration_page_password_confirm"/></label>
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
                            <input type="password" class="form-control" name="confirm" id="confirm" placeholder="<spring:message code='registration_page_password_confirm_input_title'/>"/>
                        </div>
                    </div>
                </div>

                <div class="form-group ">
                </div><button type="button" class="btn btn-primary btn-lg btn-block login-button"><spring:message code='registration_page_button_title'/></button>
                <div class="login-register">
                    <a href="/login"><spring:message code="login_page_button_title"/></a>
                </div>
            </form>
        </div>
    </div>
</div>

</body>

</html>
