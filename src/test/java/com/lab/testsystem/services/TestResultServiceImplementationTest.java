package com.lab.testsystem.services;

import com.lab.testsystem.dao.TestResultDao;
import com.lab.testsystem.dao.TestResultDaoImplementation;
import com.lab.testsystem.model.TestResult;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.sql.SQLInvalidAuthorizationSpecException;
import java.sql.SQLSyntaxErrorException;
import java.util.ArrayList;
import java.util.List;

@Log4j
public class TestResultServiceImplementationTest {

    private TestResultDao daoServiceMock;

    @Before
    public void setUp() {
        daoServiceMock = Mockito.mock(TestResultDaoImplementation.class);
    }

    @Test
    @SneakyThrows
    public void testGetWithNullObjectEmptyListExpected() {
        ArrayList<TestResult> testResults = new ArrayList<>();
        testResults.add(new TestResult());
        Mockito.when(daoServiceMock.get()).thenReturn(testResults);
        TestResultServiceImplementation implementation = new TestResultServiceImplementation(daoServiceMock);
        List<TestResult> actualResult = implementation.get();
        Assert.assertEquals(testResults, actualResult);
    }

    @Test(expected = DatabaseAccessException.class)
    @SneakyThrows
    public void testGetWithNullObjectDatabaseAccessExceptionExpected() {
        Mockito.doThrow(SQLInvalidAuthorizationSpecException.class).when(daoServiceMock).get();
        TestResultServiceImplementation implementation = new TestResultServiceImplementation(daoServiceMock);
        implementation.get();
    }

    @Test(expected = DatabaseAccessException.class)
    @SneakyThrows
    public void testGetWithNullObjectDatabaseAccessExceptionExpectedSecondCheck() {
        Mockito.doThrow(SQLException.class).when(daoServiceMock).get();
        TestResultServiceImplementation implementation = new TestResultServiceImplementation(daoServiceMock);
        implementation.get();
    }

    @Test(expected = DatabaseAccessException.class)
    @SneakyThrows
    public void testWithNullObjectDatabaseAccessExceptionExpectedThirdCheck() {
        Mockito.doThrow(SQLSyntaxErrorException.class).when(daoServiceMock).get();
        TestResultServiceImplementation implementation = new TestResultServiceImplementation(daoServiceMock);
        implementation.get();
    }

    @Test(expected = DatabaseAccessException.class)
    @SneakyThrows
    public void testGetWithNullObjectDatabaseAccessExceptionExpectedForthCheck() {
        Mockito.doThrow(SQLFeatureNotSupportedException.class).when(daoServiceMock).get();
        TestResultServiceImplementation implementation = new TestResultServiceImplementation(daoServiceMock);
        implementation.get();
    }

    @Test
    @SneakyThrows
    public void testGetWithNullObjectCertainListExpected() {
        ArrayList<TestResult> expectedResults = new ArrayList<>();
        expectedResults.add(new TestResult());
        expectedResults.add(new TestResult());
        expectedResults.add(new TestResult());
        Mockito.when(daoServiceMock.get()).thenReturn(expectedResults);
        TestResultServiceImplementation implementation = new TestResultServiceImplementation(daoServiceMock);
        List<TestResult> actualResult = implementation.get();
        Assert.assertEquals(expectedResults, actualResult);
    }

    @Test
    @SneakyThrows
    public void testSaveWithOneArgSuccessExpected() {
        Mockito.doReturn(null).when(daoServiceMock).create(Mockito.any());
        TestResultServiceImplementation implementation = new TestResultServiceImplementation(daoServiceMock);
        implementation.save(new TestResult());
        Mockito.verify(daoServiceMock, Mockito.times(1)).create(Mockito.any());
    }

    @Test(expected = IllegalArgumentException.class)
    @SneakyThrows
    public void testSaveWithNullArgumentIllegalArgumentExceptionExpected() {
        Mockito.doReturn(null).when(daoServiceMock).create(Mockito.any());
        TestResultServiceImplementation implementation = new TestResultServiceImplementation(daoServiceMock);
        implementation.save(null);
    }

    @Test(expected = DatabaseAccessException.class)
    @SneakyThrows
    public void testSaveWithNotConsistentArgumentDatabaseAccessExceptionExpected() {
        Mockito.doThrow(SQLSyntaxErrorException.class).when(daoServiceMock).create(Mockito.any());
        TestResultServiceImplementation implementation = new TestResultServiceImplementation(daoServiceMock);
        TestResult notConsistentTestResult = new TestResult();
        notConsistentTestResult.setTest(null);
        implementation.save(notConsistentTestResult);
    }

    @Test(expected = DatabaseAccessException.class)
    @SneakyThrows
    public void testSaveWithNotConsistentArgumentDatabaseAccessExceptionExpectedSecondAttempt() {
        Mockito.doThrow(SQLException.class).when(daoServiceMock).create(Mockito.any());
        TestResultServiceImplementation implementation = new TestResultServiceImplementation(daoServiceMock);
        TestResult notConsistentTestResult = new TestResult();
        notConsistentTestResult.setResults(null);
        implementation.save(notConsistentTestResult);
    }

    @Test(expected = DatabaseAccessException.class)
    @SneakyThrows
    public void testSaveWithNotConsistentArgumentDatabaseAccessExceptionExpectedThirdAttempt() {
        Mockito.doThrow(SQLInvalidAuthorizationSpecException.class).when(daoServiceMock).create(Mockito.any());
        TestResultServiceImplementation implementation = new TestResultServiceImplementation(daoServiceMock);
        TestResult notConsistentTestResult = new TestResult();
        notConsistentTestResult.setUser(null);
        implementation.save(notConsistentTestResult);
    }

    @Test(expected = DatabaseAccessException.class)
    @SneakyThrows
    public void testGetWithIllegalLongArgumentDatabaseAccessExceptionExpected() {
        Mockito.doThrow(SQLException.class).when(daoServiceMock).get(Mockito.anyLong());
        TestResultServiceImplementation implementation = new TestResultServiceImplementation(daoServiceMock);
        implementation.get(12345678L);
    }
    @Test
    @SneakyThrows
    public void testGetWithRightLongArgumentSuccessExpected() {
        Mockito.doReturn(new TestResult()).when(daoServiceMock).get(Mockito.anyLong());
        TestResultServiceImplementation implementation = new TestResultServiceImplementation(daoServiceMock);
        TestResult actual = implementation.get(12345678L);
        Assert.assertEquals(new TestResult(), actual);
    }
    @Test(expected = IllegalArgumentException.class)
    @SneakyThrows
    public void testGetWithZeroLongArgumentSuccessExpected() {
        TestResultServiceImplementation implementation = new TestResultServiceImplementation(daoServiceMock);
        implementation.get(0L);
    }


    @Test(expected = IllegalArgumentException.class)
    @SneakyThrows
    public void testDeleteWithNullObjectIllegalArgumentExceptionExpected() {
        TestResultServiceImplementation implementation = new TestResultServiceImplementation(daoServiceMock);
        implementation.delete(null);
    }

    @Test
    @SneakyThrows
    public void testDeleteWithConsistentObjectSuccessExpected() {
        Mockito.doReturn(null).when(daoServiceMock).delete(Mockito.any());
        TestResultServiceImplementation implementation = new TestResultServiceImplementation(daoServiceMock);
        implementation.delete(new TestResult());
        Mockito.verify(daoServiceMock, Mockito.times(1)).delete(Mockito.any());
    }


    @AfterClass
    public static void end() {
        log.info("all tests end!");
    }

}
